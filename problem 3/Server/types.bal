
public type StudentProfile record {

    string studentNr;

    string name;

    string email;

    Course[] courses;

};


public type Course record {

    string courseCode;

    Assessment[] assessments;

};


public type Assessment record {

    string name;

    float weight;

    float marks;

};


public type UpdateCourseRequest record {

    StudentProfile course;

};


public type UpdateStudentRequest record {

    StudentProfile user;

};


public type RemoveStudentRequest record {

    string studentNr;

};


public type SuccessResponse record {

    string message;

};


public type ErrorResponse record {

    string message;

};
