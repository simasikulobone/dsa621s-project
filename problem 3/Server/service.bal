import ballerina/http;

# A service representing a network-accessible API
# bound to port `9092`.
listener http:Listener ep0 = new (9090, config = {host: "localhost"});

// hashmap of studentProfiles
map<StudentProfile> studentProfiles = {};

service /studentService on ep0 {

    // CREATE STUDENT
    resource function post student/create(@http:Payload {} StudentProfile payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {

        // check if student already exists
        if studentProfiles.hasKey(payload.studentNr) {

            // return error message
            return {
                body: {
                    message: "User already exists"
                }
            };
        }

        // add student
        studentProfiles[payload.studentNr] = payload;

        // return success message   
        return {message: "successfully added user"};
    }

    // DELETE STUDENT 
    resource function post student/delete(@http:Payload {} RemoveStudentRequest payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {

        // CHECK IF STUDENT EXISTS
        if studentProfiles.hasKey(payload.studentNr) {

            // REMOVE STUDENT IF IT EXISTS#
            StudentProfile|error removedUser = studentProfiles.remove(payload.studentNr);

            // CHECK IF STUDENT REMOVED
            if removedUser is error {
                return {
                    body: {
                        message: "Failed to Remove User"
                    }
                };
            } else {
                return {
                    message: "Successfuly Removed User"
                };
            }
        }

        return {message: "Student not found"};
    }

    // UPDATE STUDENT ENDPOINT
    resource function post student/update(@http:Payload {} StudentProfile payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {

        // check if student exists
        if studentProfiles.hasKey(payload.studentNr) {

            // get student profile
            StudentProfile|error student = studentProfiles.get(payload.studentNr);
            if student is error {
                return {
                    body: {
                        message: "Failed to Update student"
                    }
                };

            } else {
                // update student details
                student.name = payload.name;
                student.email = payload.email;
                return {message: "Successfully Updated student"};
            }
        }

        return {message: "Student not found"};
    }

// UPDATE COURSE DETAILS ENDPOINT#
resource function post course/update(@http:Payload {} UpdateCourseRequest payload) returns SuccessResponse|record {|*http:BadRequest; ErrorResponse body;|} {
    if studentProfiles.hasKey(payload.course.code) {

        //     UPDATE COURSE DETAILS IF IT EXISTS#

        StudentCourses|error courseToUpdate = StudentCourses.get(payload.course.code);

        //       CHECK IF COURSE DETAILS IS UPDATED#

        if courseToUpdate is error {
            return {
                body: {
                    message: "Failed to Update User"
                }
            };
        } else {
            //OVERWRITE OLD COURSE DETAILS DATA WITH NEW DATA#
            courseToUpdate = payload.course;
            return {message: "Successfully Updated User"};
        }
    }
}
//      GET STUDENT BY STUDENT NUMBER ENDPOINT#
resource function get user/[string studentNr]() returns StudentProfile|record {|*http:BadRequest; ErrorResponse body;|} {

    //     CHECK IF STUDENT EXISTS#
    if StudentProfile.hasKey(studentNr) {
        //     RETURN Student IF EXISTS#    
        return StudentProfile.get(studentNr);
    }
    //     RETURN ERROR IF STUDENT DOES NOT EXIST#
    return {
        body: {
            message: "user does not exist"
        }
    };
}

}
