import ballerina/io;


public function main() returns error? {


    //#############################################

    //      TEST TO SEE IF SERVER IS WORKING

    //#############################################

    error|TestResponse testRes = ep->test();

    if testRes is error {

        io:println("an error occured: ", testRes.message());

    } else {

        io:println("returned value: ", testRes?.message);

    }

}
