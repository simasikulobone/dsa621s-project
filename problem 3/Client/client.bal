import ballerina/http;

# Successful response
public type TestResponse record {
    string message;
};

# Api to create and update Student profiles
#
# + clientEp - Connector http endpoint
public client class Client {
    http:Client clientEp;
    public isolated function init(http:ClientConfiguration clientConfig = {}, string serviceUrl = "http://localhost:9090/studentService") returns error? {
        http:Client httpEp = check new (serviceUrl, clientConfig);
        self.clientEp = httpEp;
    }
    #
    # + return - Successful response
    remote isolated function test() returns TestResponse|error {
        string path = string `/test`;
        TestResponse response = check self.clientEp->get(path, targetType = TestResponse);
        return response;
    }
# + return - Successfully added new student
    remote isolated function addUser(StudentProfile payload) returns SuccessResponse|error {
        string path = string `/addUser`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }}
    # + return - Successfully removed Student
    remote isolated function remove(RemoveStudentRequest payload) returns SuccessResponse|error {
        string path = string `/user/remove`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    #
    # + studentNr - existing user's username
    # + return - Successfully retrieved user using username
    remote isolated function userByusername(string username) returns StudentProfile|error {
        string path = string `/user/${username}`;
        StudentProfile response = check self.clientEp->get(path, targetType = StudentProfile);
        return response;
    }
    #
    # + return - Successfully updated new Student
    remote isolated function update(UpdateStudentRequest payload) returns SuccessResponse|error {
        string path = string `/user/update`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
    # + return - Successfully updated Course
    remote isolated function update(UpdateCourseRequest payload) returns SuccessResponse|error {
        string path = string `/course/update`;
        http:Request request = new;
        json jsonBody = check payload.cloneWithType(json);
        request.setPayload(jsonBody);
        SuccessResponse response = check self.clientEp->post(path, request, targetType = SuccessResponse);
        return response;
    }
