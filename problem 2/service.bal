import ballerina/graphql;

//datatypes
public type CovidEntry record {|
    readonly string isoCode;
    string date;
    string region;
    decimal deaths?;
    decimal confirmed_cases?;
    decimal recoveries?;
    decimal tested?;
|};

//data
table<CovidEntry> key(isoCode) covidEntriesTable = table [
    {isoCode: "NAM", date: "12/09/2021", region: "Khomas", deaths: 39, confirmed_cases: 465, recoveries: 67, tested: 1200}
];

//object types
public distinct service class CovidInfo {
    private final readonly & CovidEntry entryRecord;

    function init(CovidEntry entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }

    resource function get isoCode() returns string {
        return self.entryRecord.isoCode;
    }

    resource function get date() returns string {
        return self.entryRecord.date;
    }

    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get deaths() returns decimal? {
        if self.entryRecord.deaths is decimal {
            return self.entryRecord.deaths;
        }
        return;
    }

    resource function get confirmed_cases() returns decimal? {
        if self.entryRecord.confirmed_cases is decimal {
            return self.entryRecord.confirmed_cases;
        }
        return;
    }

    resource function get recoveries() returns decimal? {
        if self.entryRecord.recoveries is decimal {
            return self.entryRecord.recoveries;
        }
        return;
    }

    resource function get tested() returns decimal? {
        if self.entryRecord.tested is decimal {
            return self.entryRecord.tested;
        }
        return;
    }
}

//service and service methods

# A service representing a network-accessible API
# bound to port `9090`.  
service /covid19 on new graphql:Listener(9090) {
    resource function get all() returns CovidInfo[] {
        CovidEntry[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidInfo(entry));
    }

    resource function get filter(string isoCode) returns CovidInfo? {
        CovidEntry? covidEntry = covidEntriesTable[isoCode];
        if covidEntry is CovidEntry {
            return new (covidEntry);
        }
        return;
    }

    remote function add(CovidEntry entry) returns CovidInfo {
        covidEntriesTable.add(entry);
        return new CovidInfo(entry);
    }
}

